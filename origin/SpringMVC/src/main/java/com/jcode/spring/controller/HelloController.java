package com.jcode.spring.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

//This is a comment

@Controller
public class HelloController {
	
	
	@RequestMapping("goToHello")
	public ModelAndView resultHello(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("hello");
		mv.addObject("message","Hello, i am called from a controller");
		return mv;
	}
}

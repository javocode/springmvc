package com.jcode.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jcode.spring.model.Persona;

@Controller
public class RegistrarController {
	
	@RequestMapping("/goForm")
	public ModelAndView goToForm(){
		return new ModelAndView("formulario","command", new Persona());
	}
	
	@RequestMapping(value = "/agregar", method = {RequestMethod.GET, RequestMethod.POST})
	public String agregar(Persona persona, ModelMap model){
		model.addAttribute("nombreModel",persona.getNombre());
		model.addAttribute("apellidoModel",persona.getApellido());
		return "exito";
	}
	

}
